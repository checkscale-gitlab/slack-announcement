#!/usr/bin/env bash

ACTION="$1"
STATUS="$2"

set -e

assure_is_set () {
  VARNAME="$1"
  if [[ -z "${!VARNAME}" ]] ; then
    echo "$VARNAME is missing. Running in CI?"
    exit 1
  fi
}

usage () {
  echo "Posts deployment messages to a given Slack hook"
  echo "Call $0 {succeeded,failed,started,...} ACTION"
  exit 1
}

assure_is_set "SLACK_DEPLOY_MESSAGE_HOOK_URL"
assure_is_set "CI_TARGET_ENVIRONMENT_NAME"
assure_is_set "CI_TARGET_PROJECT_URL"
assure_is_set "CI_TARGET_PIPELINE_ID"
assure_is_set "GITLAB_USER_EMAIL"
assure_is_set "GITLAB_USER_NAME"

case "$STATUS" in
  succeeded)
    COLOR="good"
    ;;
  failed)
    COLOR="danger"
    ;;
  started)
    COLOR="warning"
    ;;
  *)
    # Slack's default plain gray
    COLOR="#dee3e5"
    ;;
esac

if [[ -z "$ACTION" || -z "$STATUS" ]] ; then
  usage
fi

echo "Posting status message to slack..."

curl -H "Content-Type: application/json" -X POST -d "
{
  \"attachments\": [
    {
      \"color\": \"$COLOR\",
      \"author_name\": \"$GITLAB_USER_NAME\",
      \"author_link\": \"mailto:$GITLAB_USER_EMAIL\",
      \"title\": \"$ACTION $STATUS\",
      \"title_link\": \"$CI_TARGET_PROJECT_URL/pipelines/$CI_TARGET_PIPELINE_ID\",
      \"text\": \"*Environment*: $CI_TARGET_ENVIRONMENT_NAME, *Pipeline*: $CI_TARGET_PIPELINE_ID *Product(s):* $DEPLOY_PRODUCTS\",
      \"mrkdwn_in\": [\"text\"],
      \"thumb_url\": \"https://s3.eu-central-1.amazonaws.com/testbrew/logos/thumb-$CI_TARGET_ENVIRONMENT_NAME.png\"
    }
  ]
}
" $SLACK_DEPLOY_MESSAGE_HOOK_URL
