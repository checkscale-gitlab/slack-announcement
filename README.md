# Slack announcement 
Project is to enable teams to send slack messages with ease.   
The projects build an image with backed-in announcement script.

### Usage
There are two different usages of this script. One uses a separate job with the built docker image. 
The second is running the script as part of the original job. the before and after scripts 
clones this project and runs the script.
   
1. [Separate steps usage](#Separate steps usage)
2. [Using before/after scripts](#Using before/after scripts)
   
#### Separate steps usage
In your CI file you will need to configure the following 
```yaml
image: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/slack-announcement:latest

stages:
  - Before
  - ...
  - After

variables:
  SLACK_DEPLOY_MESSAGE_HOOK_URL: https://hooks.slack.com/services/...
  CI_TARGET_ENVIRONMENT_NAME: "staging"
  DEPLOY_PRODUCTS: "My website, My microservice"
  SLACK_MSG: "Who is on first, What is on second..."

.Announce template:
  script:
    - /deployment_message_to_slack.sh "$SLACK_MSG" "$DEPLOYMENT_STATE"

Announce Before:
  extends: .Announce template
  stage: Before
  variables:
    DEPLOYMENT_STATE: started
.
.
.

Announce After:
  extends: .Announce template
  stage: After
  variables:
    DEPLOYMENT_STATE: succeeded

Announce Failed:
  extends: Announce After
  when: on_failure
  variables:
    DEPLOYMENT_STATE: failed
```

#### Using before/after scripts
> CI_JOB_TOKEN is part of GitLab default tokens   
   
> Note: currently there is no indication of `failed` jobs
```yaml
deploy job:
  variables:
    SCRIPTS_REPO: https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/testbrew/slack-announcement.git
    SLACK_DEPLOY_MESSAGE_HOOK_URL: https://hooks.slack.com/services/...
    DEPLOY_PRODUCTS: "TestBrew site"
    SLACK_MSG: "Deployment NoOps"
  stage: Before and after script example
  before_script:
    - export SCRIPTS_DIR=$(mktemp -d)
    - git clone -q --depth 1 "$SCRIPTS_REPO" "$SCRIPTS_DIR"
    - $SCRIPTS_DIR/deployment_message_to_slack.sh "$SLACK_MSG" started
  script:
    - echo hello world
  after_script:
    - export SCRIPTS_DIR=$(mktemp -d)
    - git clone -q --depth 1 "$SCRIPTS_REPO" "$SCRIPTS_DIR"
    - $SCRIPTS_DIR/deployment_message_to_slack.sh "$SLACK_MSG" succeeded
```



